# Minesweeper

<img src="src/assets/mine.png" width="150" alt="Logo">

[Try in your browser](https://kyrdairrin.gitlab.io/minesweeper/)

## Summary

A browser-based minesweeper game.

## Project setup

```console
yarn install
```

### Compiles and hot-reloads for development

```console
yarn serve
```

### Compiles and minifies for production

```console
yarn build
```

### Lints and fixes files

```console
yarn lint
```

### Run your unit tests

```console
yarn test:unit
```

### Run your end-to-end tests

```console
yarn test:e2e
```

## License

![AGPLv3][agpl]

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [LICENSE.txt](LICENSE.txt) for details.

[agpl]: src/assets/agplv3-medium.png

## Credits

Project icon made by [Creaticca Creative Agency](https://www.flaticon.com/authors/creaticca-creative-agency "Creaticca Creative Agency") from [www.flaticon.com](https://www.flaticon.com/ "Flaticon") is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")