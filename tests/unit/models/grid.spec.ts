import Grid from '@/models/grid';
import Tile from '@/models/tile';

describe('Grid', () => {
    it('generates good number of mines', () => {
        const grid = new Grid(12, 9, 13);

        const flattenedTiles: Tile[] = Array<Tile>().concat(...grid.tiles);
        const minesCount = flattenedTiles.filter(t => t.mined).length;

        expect(minesCount).toEqual(grid.minesCount);
    });
    it('generates distinct tile instances', () => {
        const grid = new Grid(12, 9, 13);

        const flattenedTiles: Tile[] = Array<Tile>().concat(...grid.tiles);

        const checked: Tile[] = [];
        for (const tile of flattenedTiles) {
            expect(checked).not.toContain(tile);
            checked.push(tile);
        }
    });
});
