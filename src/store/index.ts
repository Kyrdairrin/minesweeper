import Vue from 'vue';
import Vuex, { ActionContext } from 'vuex';
import IState from './state.contract';
import Grid from '@/models/grid';
import { NEW_GAME, INCREMENT_TIME, START, STOP, RESET_REMAINING, DECREMENT_REMAINING } from './mutation-types';
import { ACTION_START, ACTION_NEW_GAME, ACTION_STOP } from './action-types';

Vue.use(Vuex);

const initialState: IState = {
  grid: new Grid(10, 10, 10),
  running: false,
  time: 0,
  remainingTiles: 100,
};

let handle: number | null = null;

export default new Vuex.Store({
  state: initialState,
  mutations: {
    [NEW_GAME](state: IState) {
      state.time = 0;
      Object.assign(state.grid, new Grid(10, 10, 10));
    },
    [INCREMENT_TIME](state: IState) {
      state.time++;
    },
    [START](state: IState) {
      state.running = true;
    },
    [STOP](state: IState) {
      state.running = false;
    },
    [RESET_REMAINING](state: IState) {
      state.remainingTiles = state.grid.height * state.grid.width;
    },
    [DECREMENT_REMAINING](state: IState) {
      state.remainingTiles--;
    },
  },
  actions: {
    [ACTION_START](context) {
      context.commit(START);
      handle = setInterval(() => context.commit(INCREMENT_TIME), 1000);
    },
    [ACTION_STOP](context) {
      if (handle !== null) {
        clearInterval(handle);
        handle = null;
      }
      context.commit(STOP);
    },
    [ACTION_NEW_GAME](context) {
      context.dispatch(ACTION_STOP);
      context.commit(NEW_GAME);
      context.commit(RESET_REMAINING);
    },
  },
});
