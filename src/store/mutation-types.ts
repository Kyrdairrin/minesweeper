export const NEW_GAME = 'NEW_GAME';
export const INCREMENT_TIME = 'INCREMENT_TIME';
export const START = 'START';
export const STOP = 'STOP';
export const RESET_REMAINING = 'RESET_REMAINING';
export const DECREMENT_REMAINING = 'DECREMENT_REMAINING';
