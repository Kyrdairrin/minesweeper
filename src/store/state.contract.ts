import Grid from '@/models/grid';

export default interface IState {
    grid: Grid;
    running: boolean;
    time: number;
    remainingTiles: number;
}
