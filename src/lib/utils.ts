import Tile from '@/models/tile';

export default abstract class Utils {
    public static randomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    public static getAdjacents(tiles: Tile[][], tile: Tile): Tile[] {
        const adjacents: Tile[] = [];
        const { i, j } = tile;

        for (let di = (i > 0 ? -1 : 0); di <= (i < tiles.length - 1 ? 1 : 0); di++) {
            for (let dj = (j > 0 ? -1 : 0); dj <= (j < tiles[0].length - 1 ? 1 : 0); dj++) {
                if (di !== 0 || dj !== 0) {
                    adjacents.push(tiles[i + di][j + dj]);
                }
            }
        }

        return adjacents;
    }
}
