import Tile from './tile';
import Utils from '@/lib/utils';
import { Success } from './success';

export default class Grid {
    public tiles: Tile[][];
    public success: Success = Success.Unknown;

    constructor(public width: number, public height: number, public minesCount: number) {
        if (width <= 0 || height <= 0) {
            throw new Error('Width and height must be strictly positive.');
        }

        if (minesCount > width * height) {
            throw new Error(`Mines count (${minesCount}) must not be strictly greater than width * height (${width * height})`);
        }

        this.tiles = this.generate();
    }

    private generate(): Tile[][] {

        // If more mines than empty tiles, fill with mines and add random free tiles. If not, do the opposite.
        const tilesCount = this.height * this.width;
        const beginWithMines = this.minesCount > tilesCount / 2;

        let x = 0;
        const tiles: Tile[][] = Array.from(
            {length: this.width},
            () => {
                let y = 0;
                const inner = Array.from({length: this.height}, () => new Tile(x, y++, beginWithMines));
                x++;
                return inner;
            });

        const targetCount = beginWithMines ? tilesCount - this.minesCount : this.minesCount;

        for (let count = 0; count < targetCount; count++) {
            const i = Utils.randomInt(0, this.width - 1);
            const j = Utils.randomInt(0, this.height - 1);

            if (tiles[i][j].mined === !beginWithMines) {
                count --;
            } else {
                tiles[i][j] = new Tile(i, j, !beginWithMines);
            }
        }

        this.hint(tiles);

        return tiles;
    }

    private hint(tiles: Tile[][]): void {
        Array<Tile>().concat(...tiles).forEach(tile => {
            if (tile.hint === null) {
                const adjacents = Utils.getAdjacents(tiles, tile);
                const count = adjacents.filter(t => t.mined).length;
                tile.hint = count === 0 ? null : count.toString();
            }
        });
    }
}
