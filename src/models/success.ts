export enum Success {
    Won,
    Lost,
    Unknown,
}
