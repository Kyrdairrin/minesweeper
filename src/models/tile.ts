export default class Tile {
    public discovered: boolean = false;
    public flagged: boolean = false;
    public hint: string | null;

    constructor(public i: number, public j: number, public mined: boolean) {
        this.hint = mined ? '💣' : null;
    }
}
